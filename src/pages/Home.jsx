import React from "react";

import Hero from "../components/BannerComponent/Hero";
import Banner from "../components/BannerComponent/Banner";
import Services from "../components/HomeComponent/Services";
import RoomFeature from "../components/HomeComponent/FeaturedRooms";
// import 

const Home = () => {
	return (
    <React.Fragment>
      <Hero class="hero--home">
        <Banner
          title="luxurious rooms"
          subtitle="deluxe rooms starting at $299"
          redirect="/rooms"
          linktxt="our rooms"
        />
      </Hero>
      <Services/>
      <RoomFeature/>
    </React.Fragment>
	);
};

export default Home;

import React, { useContext } from "react";

import Hero from "../components/BannerComponent/Hero";
import Banner from "../components/BannerComponent/Banner";
import RoomFilter from "../components/RoomComponent/RoomFilter.jsx";
import RoomList from "../components/RoomComponent/RoomList";
import { RoomContext } from "../context/RoomContext";

const Room = () => {
  const { roomsState: {sortedRooms} } = useContext(RoomContext);
  
	return (
		<React.Fragment>
			<Hero class="hero--rooms">
				<Banner title="our rooms" redirect="/" linktxt="return home" />
      </Hero>
      <RoomFilter />
      <RoomList rooms = {sortedRooms}/>
		</React.Fragment>
	);
};

export default Room;

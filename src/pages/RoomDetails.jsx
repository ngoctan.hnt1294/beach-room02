import React, { useContext } from "react";

import { useParams } from "react-router-dom";
import { RoomContext } from "../context/RoomContext";
import Hero from "../components/BannerComponent/Hero";
import Banner from "../components/BannerComponent/Banner";
import AboutRoom from "../components/RoomDetailsComponent/AboutRoom";
import HeroStyle from "../components/RoomDetailsComponent/HeroStyle";

const RoomDetails = () => {
	const { slug } = useParams();
	const { roomsState: {rooms} } = useContext(RoomContext);
  const room = rooms.find((room) => room.slug === slug);
	if (!room) {
		return (
			<React.Fragment>
				<Hero class="hero--roomdetails">
					<Banner
						title="Single Standard Room"
						redirect="/rooms/"
						linktxt="back to room"
					/>
				</Hero>
			</React.Fragment>
		);
	} else {
		const {
			name,
			description,
			capacity,
			size,
			price,
			extras,
			breakfast,
			pets,
			images,
		} = room;
		const [mainImg, ...listImg] = images;
		return (
			<React.Fragment>
				<HeroStyle img={mainImg}>
					<Banner
						title="Single Standard Room"
						redirect="/rooms/"
						linktxt="back to room"
					/>
				</HeroStyle>
				<AboutRoom
					name={name}
					images={listImg}
					extras={extras}
					description={description}
          capacity={capacity}
          size={size}
          price={price}
          breakfast={breakfast}
          pets={pets}
				/>
			</React.Fragment>
		);
	}
};

export default RoomDetails;

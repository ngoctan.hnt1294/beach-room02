import React from 'react';
import { FaCocktail, FaHiking, FaShuttleVan, FaBeer } from "react-icons/fa";

import Title from './../../components/TitleComponent/Title';

import './Services.css';

const serviceList = [
  {
    icon: <FaCocktail />,
    title: "free cocktails",
    info:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, corporis!"
  },
  {
    icon: <FaHiking />,
    title: "Endless Hiking",
    info:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, corporis!"
  },
  {
    icon: <FaShuttleVan />,
    title: "Free shuttle",
    info:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, corporis!"
  },
  {
    icon: <FaBeer />,
    title: "Strongest Beer",
    info:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, corporis!"
  }
];

const Services = () => {
  const serviceItem = serviceList.map((service, index) => {
    return (
      <article key={index} className="services-item">
        <span className="services-item__ico">{service.icon}</span>
        <h3 className="services-item__ttl">{service.title}</h3>
        <p className="services-item__txt">{service.info}</p>
      </article>
    );
  });
  return(
    <div className="services">
      <Title title="services" />
      <div className="services-list">
        {serviceItem}
      </div>
    </div>
  );
}

export default Services;

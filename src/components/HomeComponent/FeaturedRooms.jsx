import React, { useContext } from "react";

import Title from "../TitleComponent/Title";
import RoomList from "../RoomComponent/RoomList";
import { RoomContext } from "../../context/RoomContext";

import "./FeaturedRooms.css";

const FeaturedRooms = () => {
	const {
		roomsState: { featuredRooms },
	} = useContext(RoomContext);
	return (
		<div className="featured-rooms">
			<Title title="featured rooms" />
      <RoomList rooms={featuredRooms} />
		</div>
	);
};

export default FeaturedRooms;

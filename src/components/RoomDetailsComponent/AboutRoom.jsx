import React from 'react';

import "./AboutRoom.css"

const AboutRoom = (props) => {
  return (
    <div className="room-about">
      <div className="room-about__img">
        {props.images.map((image, index) => {
          return <img key={index} src={image} alt={props.name} />;
        })}
      </div>
      <div className="room-about-body">
        <div className="room-about-body__detail">
          <h3>details</h3>
          <p>{props.description}</p>
        </div>
        <div className="room-about-body__info">
          <h3>Info</h3>
          <ul className="room-about-body-info__list">
            <li>Price: ${props.price}</li>
            <li>Size: {props.size}SQFT</li>
            <li>Max Capacity: {props.capacity > 1 ? `${props.capacity} people` : `${props.capacity} person`}</li>
            <li>{props.pets ? "pets allowed" : "no pets allowed"}</li>
            <li>{props.breakfast && "free breakfast included"}</li>
          </ul>
        </div>
      </div>
      <div className="room-about-extras">
        <h4>Extras</h4>
        <ul className="room-about-extras__list">
          {props.extras.map((item, index) => {
            return <li key={index}>- {item}</li>;
          })}
        </ul>
      </div>
    </div>
  );
}

export default AboutRoom;

import React from 'react';
import {Link} from 'react-router-dom';

import "./Banner.css";

const Banner = (props) => {
  return (
    <div className="banner">
      <h1 className="banner__ttl">{props.title}</h1>
      <div className="banner__line"></div>
      <p className="banner__subttl">{props.subtitle}</p>
      <Link to={props.redirect} className="btn-primary">{props.linktxt}</Link>
    </div>
  )
}

export default Banner;

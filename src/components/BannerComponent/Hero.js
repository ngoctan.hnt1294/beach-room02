import React from 'react';

import "./Hero.css";

const Hero = (props) => {
  return (
    <div className={`hero ${props.class}`}>
      {props.children}
    </div>
  );
}

export default Hero;
import React from 'react';
import { Link } from 'react-router-dom';
import defaultImg from './../../assets/images/room-1.jpeg';

import './RoomItem.css';

const RoomItem = (props) => {
  return (
    <article className="room">
      <div className="room-content">
        <img src={props.image || defaultImg} alt="" className="room-content__img"/>
        <div className="room-content__price">
          <p className="room-content__price-value">{props.price}</p>
          <p className="room-content__price-txt">per night</p>
        </div>
        <Link to={`/rooms/${props.slug}`} className="btn-primary room-content__link">Features</Link>
      </div>
      <p className="room-info">{props.name}</p>
    </article>
  );
};

export default RoomItem;
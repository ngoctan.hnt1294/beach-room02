import React, { useContext } from "react";
import { RoomContext } from "../../context/RoomContext";
import Title from "../TitleComponent/Title";
import "./RoomFilter.css";

const RoomFilter = (props) => {
	const {
		roomsState: { rooms },
		filters: {
			type,
			capacity,
			price,
			minPrice,
			maxPrice,
			minSize,
			maxSize,
			breakfast,
			pets,
		},
		handleChange,
  } = useContext(RoomContext);
  
	const getUnique = (items, value) => {
		return [...new Set(items.map((item) => item[value]))];
	};

	let types = ["all", ...getUnique(rooms, "type")];
	let capacities = getUnique(rooms, "capacity");

	const typeItem = types.map((type, index) => {
		return (
			<option key={index} value={type}>
				{type}
			</option>
		);
	});
	const guests = capacities.map((guest, index) => {
		return (
			<option key={index} value={guest}>
				{guest}
			</option>
		);
	});
	return (
		<div className="room-filter">
			<Title title="search rooms" />
			<form className="room-filter-form">
				<div className="form-group">
					<label htmlFor="type">Room Type</label>
					<select
						name="type"
						id="type"
						value={type}
						onChange={handleChange}
						className="form-control"
					>
						{typeItem}
					</select>
				</div>
				{/* form-group */}
				<div className="form-group">
					<label htmlFor="capacity">Guests</label>
					<select
						name="capacity"
						id="capacity"
						className="form-control"
						value={capacity}
						onChange={handleChange}
					>
						{guests}
					</select>
				</div>
				{/* form-group */}
				<div className="form-group">
					<label htmlFor="price">Room Price ${price}</label>
					<input
						type="range"
						name="price"
						id="price"
						min={minPrice}
						max={maxPrice}
						value={price}
						className="form-control"
						onChange={handleChange}
					/>
				</div>
				{/* form-group */}
				<div className="form-group">
					<label htmlFor="">Room Size</label>
					<div className="size-inputs">
						<input
							type="number"
							name="minSize"
							id="minSize"
							value={minSize}
							onChange={handleChange}
							className="size-inputs__item"
						/>
						<input
							type="number"
							name="maxSize"
							id="maxSize"
							value={maxSize}
							onChange={handleChange}
							className="size-inputs__item"
						/>
					</div>
				</div>
				{/* form-group */}
				<div className="form-group">
					<div className="extras-input">
						<input
							type="checkbox"
							name="breakfast"
							id="breakfast"
							checked={breakfast === true}
							onChange={handleChange}
						/>
						<label htmlFor="breakfast">breakfast</label>
					</div>
					<div className="extras-input">
						<input
							type="checkbox"
							name="pets"
							id="pets"
							checked={pets === true}
							onChange={handleChange}
						/>
						<label htmlFor="pets">pets</label>
					</div>
				</div>
				{/* form-group */}
			</form>
		</div>
	);
};

export default RoomFilter;

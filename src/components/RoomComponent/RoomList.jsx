import React from "react";

import RoomItem from "./RoomItem";

import "./RoomList.css";

const RoomList = (props) => {
	const rooms = props.rooms.map((room) => {
		return (
			<RoomItem
				key={room.id}
				image={room.images[0]}
				price={room.price}
        slug={room.slug}
        name={room.name}
			/>
		);
	});
	return (
		<div className="rooms-list">
			{rooms}
		</div>
	);
};

export default RoomList;

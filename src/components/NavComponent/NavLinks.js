import React from 'react';
import { NavLink } from "react-router-dom";

import './NavLinks.css';

const NavLinks = () => {
  return (
    <ul className="nav-links">
      <li className="nav-links__item">
        <NavLink to="/" exact>Home</NavLink>
      </li>
      <li className="nav-links__item">
        <NavLink to="/rooms" exact>Rooms</NavLink>
      </li>
    </ul>
  );
}

export default NavLinks;

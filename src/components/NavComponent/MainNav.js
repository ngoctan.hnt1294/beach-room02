import React, { useState } from "react";
import { Link } from "react-router-dom";
import logo from "./../../assets/images/logo.svg";

import Header from "./Header";
import SlideDrawer from "./SlideDrawer";
import NavLinks from "./NavLinks";

import "./MainNav.css";

const MainNav = () => {
	const [menuIsOpen, setMenuIsOpen] = useState(false);
	const openMenu = () => {
		setMenuIsOpen(true);
	};
	const closeMenu = () => {
		setMenuIsOpen(false);
	};

	return (
		<React.Fragment>
			<Header>
				<h1 className="main-nav__ttl">
					<Link to={``}>
						<img src={logo} alt="" />
					</Link>
				</h1>
				<button
					className="main-nav__menu-btn"
					onClick={!menuIsOpen ? openMenu : closeMenu}
				>
					<span />
					<span />
					<span />
        </button>
        <nav className="main-nav__nav">
          <NavLinks />
        </nav>
        <SlideDrawer show={menuIsOpen}>
          <nav className="main-nav__drawer-nav">
            <NavLinks />
          </nav>
        </SlideDrawer>
			</Header>
		</React.Fragment>
	);
};

export default MainNav;

import React from "react";
import { CSSTransition } from "react-transition-group";

import "./SlideDrawer.css";

const SlideDrawer = (props) => {
	return (
		<CSSTransition
      in={props.show}
      transitionName="slide"
			timeout={300}
			classNames="slide-in-bottom"
			mountOnEnter
			unmountOnExit
		>
			<div className="slide-drawer">{props.children}</div>
		</CSSTransition>
	);
};

export default SlideDrawer;

import React from 'react';

import './Title.css';

const Title = (props) => {
  return (
    <div className="section-ttl">
      <h2 className="section-ttl__txt">{props.title}</h2>
      <div className="section-ttl__line"></div>
    </div>
  );
};

export default Title;
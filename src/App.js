import React from "react";

import {
	BrowserRouter as Router,
	Route,
	Switch,
} from "react-router-dom";

import MainNav from './components/NavComponent/MainNav';
import Home from './pages/Home';
import Rooms from './pages/Rooms';
import Error from './pages/Error';
import RoomDetails from './pages/RoomDetails';
import RoomProvider from './context/RoomContext';

import "./App.css";

const App = () => {
	return (
    <RoomProvider>
      <Router>
        <MainNav/>
        <main>
          <Switch>
            {/* home-page */}
            <Route path="/" exact >
              <Home/>
            </Route>
            {/* END: home-page */}
            {/* rooms-page */}
            <Route path="/rooms/" exact >
              <Rooms/>
            </Route>
            {/* END: rooms-page */}
            {/* room-details-page */}
            <Route path="/rooms/:slug" exact >
              <RoomDetails/>
            </Route>
            {/* END: room-details-page */}
            {/* error-page */}
            <Route path="*">
              <Error />
            </Route>
          </Switch>
        </main>
      </Router>
    </RoomProvider>
	);
};

export default App;

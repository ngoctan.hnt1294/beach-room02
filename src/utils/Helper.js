const formatData = (data) => {
  let templateItem = data.map(item => {
    let id = item.sys.id;
    let images = item.fields.images.map(image => image.fields.file.url);
    let room = {...item.fields, images, id};
    return room;
  });
  return templateItem;
}

const featuredRoom = (data) => {
  return data.filter(item => {
    return item.featured === true;
  });
}

export {formatData, featuredRoom};
import React, {
	createContext,
	useEffect,
	useLayoutEffect,
	useState,
} from "react";
import data from "../assets/data";
import { featuredRoom, formatData } from "../utils/Helper";

export const RoomContext = createContext();

const RoomProvider = (props) => {
	let [roomsState, setRoomsState] = useState({
		rooms: [],
		sortedRooms: [],
		featuredRooms: [],
	});

	let [filters, setFilters] = useState({
		type: "all",
		capacity: 1,
		price: 0,
		minPrice: 0,
		maxPrice: 0,
		minSize: 0,
		maxSize: 0,
		breakfast: false,
		pets: false,
	});

	useEffect(() => {
		const formatRooms = formatData(data);
		const featuredRooms = featuredRoom(formatRooms);
		const maxPrice = Math.max(...formatRooms.map((item) => item.price));
		const maxSize = Math.max(...formatRooms.map((item) => item.size));

		setRoomsState(roomsState => ({
      ...roomsState,
      rooms: formatRooms,
      featuredRooms,
      sortedRooms: formatRooms,
    }));
    
    setFilters(filters => ({
      ...filters,
      maxPrice: maxPrice,
      price: maxPrice,
      maxSize: maxSize,
    }));
		return () => {};
  }, []);
  
	const handleChange = (event) => {
		const target = event.target;
		const value =
			event.target.type === "checkbox" ? target.checked : target.value;
		const name = event.target.name;
		setFilters({ ...filters, [name]: value });
	};

	useLayoutEffect(() => {
		const { rooms } = roomsState;
		let newRooms = [...rooms];
		const {
			type,
			capacity,
			price,
			minSize,
			maxSize,
			breakfast,
			pets,
    } = filters;

		if (type !== "all") {
			newRooms = newRooms.filter((room) => room.type === type);
		}

		if (capacity !== 1) {
			newRooms = newRooms.filter((room) => room.capacity >= capacity);
		}

		newRooms = newRooms.filter((room) => room.price <= price);

		newRooms = newRooms.filter(
			(room) => room.size >= minSize && room.size <= maxSize
		);

		if (breakfast) {
			newRooms = newRooms.filter((room) => room.breakfast === true);
		}

		if (pets) {
			newRooms = newRooms.filter((room) => room.pets === true);
		}

		setRoomsState(roomsState => ({
			...roomsState,
			sortedRooms: newRooms,
		}));
		return () => {};
	}, [filters]);

	return (
		<RoomContext.Provider
			value={{ roomsState, filters, handleChange: handleChange }}
		>
			{props.children}
		</RoomContext.Provider>
	);
};

export default RoomProvider;
